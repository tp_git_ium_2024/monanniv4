# TP3: Travailler et sauvegarder sur un dépôt distant



## 1- Créer un nouveau depot "MonAnniv4" :

![Q1](Screenshots/TP3 CREATE.png)

## 2- Clôner ce dépôt:

![Q2](Screenshots/TP3 Q2.png)

## 3- Créer les fichiers Invitations.txt et IdeesCadeaux.txt:

![Q3](Screenshots/TP3 Q3.png)

## 4- Les ajouter à l'index:

![Q4](Screenshots/TP3 Q4.png)

## 5- Commiter cette nouvelle (et première) version:

![Q5](Screenshots/TP3 Q5.png)

## 6- Propagez les modifications sur le dépôt distant:
![Q6](Screenshots/TP3 Q6.png)

## 7- Créez et remplissez un nouveau fichier IdeesMenu.txt:
![Q7](Screenshots/TP3 Q7.png)

## 8- Faites le nécessaire pour propager cette nouvelle version sur le dépôt distant:
![Q8](Screenshots/TP3 Q8.png)